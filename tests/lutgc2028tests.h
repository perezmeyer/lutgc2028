#ifndef LUTGC2028TESTS_H
#define LUTGC2028TESTS_H
#include <QObject>
#include <QByteArray>
#include <QtTest/QTest>

#include "../src/lutgc2028.h"

class LutGC2028Tests : public QObject
{
    Q_OBJECT

private slots:
    void checkParseSerialDataTemperature_data();
    void checkParseSerialDataTemperature();

    void checkParseSerialDataPPM_data();
    void checkParseSerialDataPPM();
};

#endif // LUTGC2028TESTS_H
