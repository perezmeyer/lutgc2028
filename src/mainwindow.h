#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStackedWidget>

#include "mainlayout.h"

class QCloseEvent;
class QAction;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);

protected:
    void closeEvent(QCloseEvent * event);

private slots:
    void about();
    void enableSaveAction(bool enable);

private:
    MainLayout * mMainLayout;
    QAction * mSaveAct;
};

#endif // MAINWINDOW_H
