#include <QTest>
#include <QSignalSpy>
#include <QDebug>

#include "../src/lutgc2028.h"

#include "lutgc2028tests.h"

void LutGC2028Tests::checkParseSerialDataTemperature_data()
{
    QTest::addColumn<QByteArray>("rawData");
    QTest::addColumn<QString>("tempValue");
    QTest::addColumn<LutGC2028::TempUnit>("tempUnit");

    char temp[16] =
    {
        0x02, '4', '2', '0', '1',
        '0', '0',
        '0', '0', '0', '0', '0', '0', '2', '5',
        '\r'
    };

    QTest::addRow("25C") << QByteArray(temp, 16) << QString("25") << LutGC2028::TempUnit::DegC;

    temp[6] = '1';
    temp[12] = '2';
    temp[13] = '5';
    temp[14] = '1';
    QTest::addRow("25.1C") << QByteArray(temp, 16) << QString("25.1") << LutGC2028::TempUnit::DegC;

    temp[6] = '2';
    temp[11] = '2';
    temp[12] = '5';
    temp[13] = '1';
    temp[14] = '2';
    QTest::addRow("25.12C") << QByteArray(temp, 16) << QString("25.12") << LutGC2028::TempUnit::DegC;

    temp[6] = '3';
    temp[10] = '2';
    temp[11] = '5';
    temp[12] = '1';
    temp[13] = '2';
    temp[14] = '3';
    QTest::addRow("25.123C") << QByteArray(temp, 16) << QString("25.123") << LutGC2028::TempUnit::DegC;

    // Negative ºC
    temp[5] = '1';
    temp[6] = '0';
    temp[10] = '0';
    temp[11] = '0';
    temp[12] = '0';
    temp[13] = '2';
    temp[14] = '5';
    QTest::addRow("-25C") << QByteArray(temp, 16) << QString("-25") << LutGC2028::TempUnit::DegC;

    temp[6] = '1';
    temp[12] = '2';
    temp[13] = '5';
    temp[14] = '1';
    QTest::addRow("-25.1C") << QByteArray(temp, 16) << QString("-25.1") << LutGC2028::TempUnit::DegC;

    temp[6] = '2';
    temp[11] = '2';
    temp[12] = '5';
    temp[13] = '1';
    temp[14] = '2';
    QTest::addRow("-25.12C") << QByteArray(temp, 16) << QString("-25.12") << LutGC2028::TempUnit::DegC;

    temp[6] = '3';
    temp[10] = '2';
    temp[11] = '5';
    temp[12] = '1';
    temp[13] = '2';
    temp[14] = '3';
    QTest::addRow("-25.123C") << QByteArray(temp, 16) << QString("-25.123") << LutGC2028::TempUnit::DegC;

    // Positive ºF
    temp[4] = '2';
    temp[5] = '0';
    temp[6] = '0';
    temp[10] = '0';
    temp[11] = '0';
    temp[12] = '0';
    temp[13] = '2';
    temp[14] = '5';
    QTest::addRow("25F") << QByteArray(temp, 16) << QString("25") << LutGC2028::TempUnit::DegF;

    temp[6] = '1';
    temp[12] = '2';
    temp[13] = '5';
    temp[14] = '1';
    QTest::addRow("25.1F") << QByteArray(temp, 16) << QString("25.1") << LutGC2028::TempUnit::DegF;

    temp[6] = '2';
    temp[11] = '2';
    temp[12] = '5';
    temp[13] = '1';
    temp[14] = '2';
    QTest::addRow("25.12F") << QByteArray(temp, 16) << QString("25.12") << LutGC2028::TempUnit::DegF;

    temp[6] = '3';
    temp[10] = '2';
    temp[11] = '5';
    temp[12] = '1';
    temp[13] = '2';
    temp[14] = '3';
    QTest::addRow("25.123C") << QByteArray(temp, 16) << QString("25.123") << LutGC2028::TempUnit::DegF;

    // Negative ºF
    temp[4] = '2';
    temp[5] = '1';
    temp[6] = '0';
    temp[10] = '0';
    temp[11] = '0';
    temp[12] = '0';
    temp[13] = '2';
    temp[14] = '5';
    QTest::addRow("-25F") << QByteArray(temp, 16) << QString("-25") << LutGC2028::TempUnit::DegF;

    temp[6] = '1';
    temp[12] = '2';
    temp[13] = '5';
    temp[14] = '1';
    QTest::addRow("-25.1F") << QByteArray(temp, 16) << QString("-25.1") << LutGC2028::TempUnit::DegF;

    temp[6] = '2';
    temp[11] = '2';
    temp[12] = '5';
    temp[13] = '1';
    temp[14] = '2';
    QTest::addRow("-25.12F") << QByteArray(temp, 16) << QString("-25.12") << LutGC2028::TempUnit::DegF;

    temp[6] = '3';
    temp[10] = '2';
    temp[11] = '5';
    temp[12] = '1';
    temp[13] = '2';
    temp[14] = '3';
    QTest::addRow("-25.123C") << QByteArray(temp, 16) << QString("-25.123") << LutGC2028::TempUnit::DegF;
}

void LutGC2028Tests::checkParseSerialDataTemperature()
{
    QFETCH(QByteArray, rawData);
    QFETCH(QString, tempValue);
    QFETCH(LutGC2028::TempUnit, tempUnit);

    auto lut = new LutGC2028(this);

    QSignalSpy spy(lut, SIGNAL(newTemperature(const QString, const LutGC2028::TempUnit)));

    lut->_buffer = rawData;
    lut->parseData();

    QVERIFY2(spy.count() == 1, "Only one signal triggered");

    QList<QVariant> values = spy.takeFirst();
    QVERIFY2(values.size() == 2, "Only two values");

    QVERIFY2(values.at(0).toString() == tempValue, "Expected temperature value");
    QVERIFY2(qvariant_cast<LutGC2028::TempUnit>(values.at(1).toInt()) == tempUnit, "Expected unit.");
}

void LutGC2028Tests::checkParseSerialDataPPM_data()
{
    QTest::addColumn<QByteArray>("rawData");
    QTest::addColumn<QString>("ppm");

    char ppm[16] =
    {
        0x02, '4', '1', '1', '9',
        '0', '0',
        '0', '0', '0', '0', '0', '7', '7', '5',
        '\r'
    };

    QTest::addRow("775PPM") << QByteArray(ppm, 16) << QString("775");

    ppm[6] = '1';
    ppm[11] = '7';
    ppm[12] = '7';
    ppm[13] = '5';
    ppm[14] = '1';
    QTest::addRow("775.1PPM") << QByteArray(ppm, 16) << QString("775.1");

    ppm[6] = '2';
    ppm[10] = '7';
    ppm[11] = '7';
    ppm[12] = '5';
    ppm[13] = '1';
    ppm[14] = '2';
    QTest::addRow("775.12PPM") << QByteArray(ppm, 16) << QString("775.12");

    ppm[6] = '3';
    ppm[9] = '7';
    ppm[10] = '7';
    ppm[11] = '5';
    ppm[12] = '1';
    ppm[13] = '2';
    ppm[14] = '3';
    QTest::addRow("775.123PPM") << QByteArray(ppm, 16) << QString("775.123");
}

void LutGC2028Tests::checkParseSerialDataPPM()
{
    QFETCH(QByteArray, rawData);
    QFETCH(QString, ppm);

    auto lut = new LutGC2028(this);

    QSignalSpy spy(lut, SIGNAL(newPPM(const QString)));

    lut->_buffer = rawData;
    lut->parseData();

    QVERIFY2(spy.count() == 1, "Only one signal triggered");

    QList<QVariant> values = spy.takeFirst();
    QVERIFY2(values.size() == 1, "Only one value");

    QVERIFY2(values.at(0).toString() == ppm, "Expected ppm value");
}

QTEST_MAIN(LutGC2028Tests)
#include "lutgc2028tests.moc"
