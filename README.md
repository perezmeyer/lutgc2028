# Lutron GC-2028 CO2 datalogger

This software is intended to be used with the Lutron GC-2028 Carbon dioxide meter
trough it's serial port.

# Requirements

- Qt >= 5.11

# Manual

The manual can be found at https://www.manualslib.com/manual/1279996/Lutron-Electronics-Gc-2028.html

# Serial cable

As per manual:

| 3.5mm jack plug | DB-9 connector | 2.2KOhms R |
|-----------------|----------------|------------|
| Center pin      | Pin 4          |            |
| Ground/shield   | Pin 2          | X          |
|                 | Pin 5          | X          |

The resistor goes between pins 2 (RX) and 5 (GND) of the DB-9 connector.

It seems the device uses a "dry contact" interface for the serial port.
I needed to use a full RS-232 serial-USB converter in order to make it work,
using a USB-3.3V converter will simply not work.
