<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>MainLayout</name>
    <message>
        <location filename="../mainlayout.ui" line="14"/>
        <source>Form</source>
        <translation>Formulario</translation>
    </message>
    <message>
        <location filename="../mainlayout.ui" line="28"/>
        <location filename="../mainlayout.cpp" line="44"/>
        <source>CO2 [PPM]</source>
        <translation>CO2 [PPM]</translation>
    </message>
    <message>
        <location filename="../mainlayout.ui" line="44"/>
        <location filename="../mainlayout.cpp" line="44"/>
        <source>Temperature</source>
        <translation>Temperatura</translation>
    </message>
    <message>
        <location filename="../mainlayout.ui" line="59"/>
        <source>0000</source>
        <translation>0000</translation>
    </message>
    <message>
        <location filename="../mainlayout.ui" line="74"/>
        <source>00.0 ºC</source>
        <translation>00.0 ºC</translation>
    </message>
    <message>
        <location filename="../mainlayout.cpp" line="60"/>
        <source>There are unsaved changes. Would you like to save them?</source>
        <translation>Hay cambios sin guardar ¿Desea guardarlos?</translation>
    </message>
    <message>
        <location filename="../mainlayout.cpp" line="62"/>
        <source>Unsaved changes</source>
        <translation>Cambios sin guardar</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="vanished">Fecha</translation>
    </message>
    <message>
        <location filename="../mainlayout.cpp" line="44"/>
        <source>Unit</source>
        <translation>Unidad</translation>
    </message>
    <message>
        <location filename="../mainlayout.cpp" line="44"/>
        <source>Epoch</source>
        <translation>Epoch</translation>
    </message>
    <message>
        <location filename="../mainlayout.cpp" line="101"/>
        <source>ºC</source>
        <translation>ºC</translation>
    </message>
    <message>
        <location filename="../mainlayout.cpp" line="101"/>
        <source>ºF</source>
        <translation>ºF</translation>
    </message>
    <message>
        <location filename="../mainlayout.cpp" line="125"/>
        <source>Serial port error</source>
        <translation>Error en el puerto serie</translation>
    </message>
    <message>
        <location filename="../mainlayout.cpp" line="126"/>
        <source>An error has occurred with the serial port. The error is: %1</source>
        <translation>Ha ocurrido un error con el puerto serie. El error es: %1</translation>
    </message>
    <message>
        <location filename="../mainlayout.cpp" line="137"/>
        <source>Generate CSV</source>
        <translation>Generar CSV</translation>
    </message>
    <message>
        <location filename="../mainlayout.cpp" line="139"/>
        <source>CSV (*.csv *.CSV)</source>
        <translation>CSV (*.csv *.CSV)</translation>
    </message>
    <message>
        <location filename="../mainlayout.cpp" line="164"/>
        <source>Could not save file</source>
        <translation>No se pudo guardar el archivo</translation>
    </message>
    <message>
        <location filename="../mainlayout.cpp" line="165"/>
        <source>An error has occured while trying to save the file %1</source>
        <translation>Un error ha ocurrido al intentar guardar el archivo %1</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="28"/>
        <source>&amp;File</source>
        <translation>&amp;Archivo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="29"/>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="33"/>
        <source>&amp;Save</source>
        <translation>&amp;Guardar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="35"/>
        <source>Save file to disk</source>
        <translation>Guardar el archivo a disco</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="43"/>
        <source>E&amp;xit</source>
        <translation>Sa&amp;lir</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="45"/>
        <source>Exit the application</source>
        <translation>Salir de pla aplicación</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="48"/>
        <source>&amp;Help</source>
        <translation>A&amp;yuda</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="49"/>
        <source>&amp;About</source>
        <translation>A&amp;cerca de</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="50"/>
        <source>Show the application&apos;s About box</source>
        <translation>Muestra la ventana &quot;Acerca de&quot;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="52"/>
        <source>About &amp;Qt</source>
        <translation>Acerca de &amp;Qt</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="53"/>
        <source>Show the Qt library&apos;s About box</source>
        <translation>Muestra la venta de información sobre Qt</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="67"/>
        <source>About lutgc2028</source>
        <translation>Sobre lutgc20208</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="68"/>
        <source>The &lt;b&gt;lutgc2028&lt;/b&gt; application is used to retrieve data from a Lutron GC-2028 and generating a CSV file.&lt;br/&gt;Current version: %1</source>
        <translation>La aplicación &lt;b&gt;lutgc2028&lt;/b&gt; se usa para obtener datos de un equipo Lutron GC-2028 y generar un archivo CSV.&lt;br/&gt;Versión actual: %1</translation>
    </message>
</context>
</TS>
