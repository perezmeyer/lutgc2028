#include <QApplication>
#include <QTranslator>
#include <QLibraryInfo>
#include <QLocale>
#include <QTextStream>

#include "mainwindow.h"

int main(int argc, char *argv[])
{
   QApplication app(argc, argv);

   // Basic app info.
   app.setOrganizationName(QStringLiteral("LisandroDNPerezMeyer"));
   app.setOrganizationDomain(QStringLiteral("perezmeyer.com.ar"));
   app.setApplicationName("lutgc2028");
   app.setApplicationVersion(LUTGC2028_VERSION);

   QTranslator qtTranslator;
   qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
   app.installTranslator(&qtTranslator);

   QTranslator qtBaseTranslator;
   qtBaseTranslator.load("qtbase_" + QLocale::system().name(),
                         QLibraryInfo::location(QLibraryInfo::TranslationsPath));
   app.installTranslator(&qtBaseTranslator);

   QTranslator appTranslator;
   /*
    *  Try loading the translations from the system's normal path first, else
    *  at the same path as the binary itself.
    */
   QString translationPath = QString("%1/share/lutgc2028/qm/lutgc2028_").arg(INSTALL_PREFIX);
   if(!appTranslator.load(translationPath + QLocale::system().name()))
       appTranslator.load("lutgc2028_" + QLocale::system().name());
   app.installTranslator(&appTranslator);

   MainWindow * window = new MainWindow();
   window->setMinimumSize(640, 480);
   window->show();

   return app.exec();
}
