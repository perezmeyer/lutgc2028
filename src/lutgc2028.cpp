#include <QByteArray>
#include <QString>
#include <QStringList>
#include <QSerialPort>
#include <QSettings>
#include <QMetaEnum>
#include <QRegExp>

#include "lutgc2028.h"

LutGC2028::LutGC2028(QObject *parent) : QObject(parent)
{
    QSettings settings;
    settings.beginGroup("LutGC2028");

    QString serialPortPath = settings.value("serialPort").toString();
    if(serialPortPath.isEmpty())
    {
        serialPortPath = defaultSerialPort();
        settings.setValue("serialPort", serialPortPath);
    }

    _serialPort = new QSerialPort(serialPortPath, this);
    connect(_serialPort, &QSerialPort::readyRead, this, &LutGC2028::readData);
    connect(_serialPort, &QSerialPort::errorOccurred, this, &LutGC2028::handleSerialPortError);

    _buffer.clear();
}

LutGC2028::~LutGC2028()
{
    _serialPort->close();
}

bool LutGC2028::open()
{
    if(!_serialPort->open(QIODevice::ReadOnly))
    {
        emit error(Error::CouldNotOpenDevice);
        return false;
    }

    return true;
}

/**
 * @brief LutGC2028::readData reads data from serial port, loads it into the
 * buffer and parses it.
 */
void LutGC2028::readData()
{
    _buffer.append(_serialPort->readAll());
    parseData();
}

void LutGC2028::parseData()
{
    // Clean up the buffer until we have 0x2.
    if(!_buffer.contains(0x2))
    {
        _buffer.clear();
        return;
    }

    // Put the start byte at the beginning.
    _buffer = _buffer.right(_buffer.size() - _buffer.indexOf(0x2));

    // Check we have all the frame.
    if(_buffer.size() < 16)
        return;

    do {
        // Second byte must be '4'.
        if(_buffer.at(1) != '4')
        {
            // Discard the frame.
            _buffer = _buffer.right(_buffer.size() - 16);
            return;
        }

        // The last byte should be \r.
        if(_buffer.at(15) != '\r')
        {
            // Discard the frame.
            _buffer = _buffer.right(_buffer.size() - 16);
            return;
        }

        if(_buffer.at(3) == '0')
        {
            // Temperature.
            QString t;
            TempUnit unit = TempUnit::DegC;

            if(_buffer.at(4) == '1')
            {
                // ºC
                unit = TempUnit::DegC;
            }
            else if(_buffer.at(4) == '2')
            {
                // ºF
                unit = TempUnit::DegF;
            }

            t = _buffer.mid(7,8);

            if((_buffer.at(6) < '0') || (_buffer.at(6) > '3'))
            {
                // Discard the frame.
                _buffer = _buffer.right(_buffer.size() - 16);
                return;
            }

            if(_buffer.at(6) != '0')
            {
                t.insert(8 - (_buffer.at(6) - 0x30), '.');
            }

            while((t.size()) > 1 && (t.startsWith('0')))
                t = t.right(t.size() - 1);

            if(_buffer.at(5) == '1')
                t.prepend('-');

            emit newTemperature(t, unit);
        }
        else if((_buffer.at(3) == '1') && (_buffer.at(4) == '9') && (_buffer.at(5) == '0'))
        {
            // PPM
            QString t = _buffer.mid(7,8);

            if((_buffer.at(6) < '0') || (_buffer.at(6) > '3'))
            {
                // Discard the frame.
                _buffer = _buffer.right(_buffer.size() - 16);
                return;
            }

            if(_buffer.at(6) != '0')
            {
                t.insert(8 - (_buffer.at(6) - 0x30), '.');
            }

            while((t.size()) > 1 && (t.startsWith('0')))
                t = t.right(t.size() - 1);

            emit newPPM(t);
        }

        // Discard the frame.
        _buffer = _buffer.right(_buffer.size() - 16);
    } while(_buffer.size() >= 16);
}

void LutGC2028::handleSerialPortError(QSerialPort::SerialPortError error)
{
    if(error == QSerialPort::SerialPortError::NoError)
        return;

    QMetaEnum metaEnum = QMetaEnum::fromType<QSerialPort::SerialPortError>();
    QString errorMsg = QString("%1").arg(metaEnum.valueToKey(error));

    emit serialPortError(errorMsg);
}

QString LutGC2028::defaultSerialPort() const
{
    return QStringLiteral("/dev/ttyUSB0");
}
