#ifndef MAINLAYOUT_H
#define MAINLAYOUT_H

#include <QWidget>
#include <QString>
#include <QMap>
#include <QSerialPort>

#include "lutgc2028.h"

class QKeyEvent;

namespace Ui {
class MainLayout;
}

class MainLayout : public QWidget
{
    Q_OBJECT

public:
    explicit MainLayout(QWidget *parent = nullptr);
    ~MainLayout();

    void close();

signals:
    void unsavedData(bool);

public slots:
    void saveReadings();

private slots:
    void refreshTable();
    void handleTemperature(const QString temperature, const LutGC2028::TempUnit unit);
    void handlePPM(QString ppm);
    void handleSerialPortError(QString & error);

private:
    LutGC2028 * _lut;
    Ui::MainLayout *ui;
    QString _fileName;
    QMap<qint64, QString> _readEntries;
    bool _unsavedData;
    bool _ppmPresent;
};

#endif // MAINLAYOUT_H
