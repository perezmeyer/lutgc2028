#ifndef LUTGC2028_H
#define LUTGC2028_H

#include <QObject>
#include <QByteArray>
#include <QSerialPort>

class LutGC2028 : public QObject
{
    Q_OBJECT
public:

    enum class Error {
        CouldNotOpenDevice,
    };

    enum class TempUnit {
        DegC,
        DegF
    };

    explicit LutGC2028(QObject *parent = nullptr);
    ~LutGC2028();

    bool open();

signals:
    void newPPM(const QString ppm);
    void newTemperature(const QString temperature, const LutGC2028::TempUnit unit);
    void error(const Error error);
    void serialPortError(QString & error);

private slots:
    void readData();
    void parseData();
    void handleSerialPortError(QSerialPort::SerialPortError error);

private:
    QString defaultSerialPort() const;

    QSerialPort * _serialPort;
    QByteArray _buffer;

    friend class LutGC2028Tests;
};

Q_DECLARE_METATYPE(LutGC2028::TempUnit);

#endif // LUTGC2028_H
