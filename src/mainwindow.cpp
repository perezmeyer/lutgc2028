#include <QApplication>
#include <QStatusBar>
#include <QMainWindow>
#include <QMenu>
#include <QToolBar>
#include <QIcon>
#include <QAction>
#include <QMenuBar>
#include <QMessageBox>
#include <QCloseEvent>
#include <QKeySequence>

#include <QDebug>

#include "lutgc2028.h"

#include "mainlayout.h"

#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    mMainLayout = new MainLayout(this);
    setCentralWidget(mMainLayout);

    // File menu and toolbar.
    QMenu * fileMenu = menuBar()->addMenu(tr("&File"));
    QToolBar * fileToolBar = addToolBar(tr("File"));

    // Save action.
    const QIcon saveIcon = QIcon::fromTheme("document-save", QIcon(":/images/save.png"));
    mSaveAct = new QAction(saveIcon, tr("&Save"), this);
    mSaveAct->setShortcuts(QKeySequence::Save);
    mSaveAct->setStatusTip(tr("Save file to disk"));
    connect(mSaveAct, &QAction::triggered, mMainLayout, &MainLayout::saveReadings);
    fileMenu->addAction(mSaveAct);
    fileToolBar->addAction(mSaveAct);

    // Exit action on menu.
    fileMenu->addSeparator();
    const QIcon exitIcon = QIcon::fromTheme("application-exit");
    QAction *exitAct = fileMenu->addAction(exitIcon, tr("E&xit"), this, &QWidget::close);
    exitAct->setShortcuts(QKeySequence::Quit);
    exitAct->setStatusTip(tr("Exit the application"));

    // Help menu.
    QMenu * helpMenu = menuBar()->addMenu(tr("&Help"));
    QAction * aboutAct = helpMenu->addAction(tr("&About"), this, &MainWindow::about);
    aboutAct->setStatusTip(tr("Show the application's About box"));

    QAction *aboutQtAct = helpMenu->addAction(tr("About &Qt"), qApp, &QApplication::aboutQt);
    aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));

    mSaveAct->setDisabled(true);
    connect(mMainLayout, &MainLayout::unsavedData, this, &MainWindow::enableSaveAction);
}

void MainWindow::closeEvent(QCloseEvent * event)
{
    mMainLayout->close();
    event->accept();
}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About lutgc2028"),
             tr("The <b>lutgc2028</b> application is used to retrieve "
                "data from a Lutron GC-2028 and generating a "
                "CSV file.<br/>Current version: %1").arg(LUTGC2028_VERSION));
}

void MainWindow::enableSaveAction(bool enable)
{
    mSaveAct->setEnabled(enable);
}
