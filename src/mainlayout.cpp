#include <QWidget>
#include <QString>
#include <QProcess>
#include <QFile>
#include <QPushButton>
#include <QMessageBox>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QMapIterator>
#include <QEvent>
#include <QKeyEvent>
#include <QStandardPaths>
#include <QDateTime>
#include <QFileDialog>
#include <QRegularExpression>
#include <QRegularExpressionMatch>

#include <QDebug>

#include "lutgc2028.h"

#include "mainlayout.h"
#include "ui_mainlayout.h"

MainLayout::MainLayout(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainLayout),
    _unsavedData(false),
    _ppmPresent(false)
{
    ui->setupUi(this);

    _lut = new LutGC2028(this);

    connect(_lut, &LutGC2028::newTemperature, this, &MainLayout::handleTemperature);
    connect(_lut, &LutGC2028::newPPM, this, &MainLayout::handlePPM);
    connect(_lut, &LutGC2028::serialPortError, this, &MainLayout::handleSerialPortError);

    _lut->open();

    ui->tableWidget->setColumnCount(4);
    ui->tableWidget->setRowCount(0);
    QStringList tableHeaders;
    tableHeaders << tr("Epoch") << tr("CO2 [PPM]") << tr("Temperature") << tr("Unit");
    ui->tableWidget->setHorizontalHeaderLabels(tableHeaders);
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

    setFocusPolicy(Qt::StrongFocus);
}

MainLayout::~MainLayout()
{
    delete ui;
}

void MainLayout::close()
{
    if(_unsavedData)
    {
        QString msg = tr("There are unsaved changes. Would you like to save them?");

        if(QMessageBox::warning(this, tr("Unsaved changes"), msg,
                                QMessageBox::Save|QMessageBox::Close) == QMessageBox::Save)
        {
            saveReadings();
        }
    }
}

void MainLayout::refreshTable()
{
    ui->tableWidget->clearContents();
    ui->tableWidget->setRowCount(_readEntries.size());

    QTableWidgetItem * item;
    QMapIterator<qint64, QString> i(_readEntries);

    int row = 0;

    while(i.hasNext())
    {
        i.next();

        QStringList parts = i.value().split(',');

        for(int column = 0; column < 4; column++)
        {
            item = new QTableWidgetItem(parts.at(column));
            ui->tableWidget->setItem(row, column, item);
        }

        row++;
    }
}

void MainLayout::handleTemperature(const QString temperature, const LutGC2028::TempUnit unit)
{
    if(!_ppmPresent)
        return;

    QString unitText = unit == LutGC2028::TempUnit::DegC ? tr("ºC") : tr("ºF");

    ui->temperatureValueLabel->setText(QString("%1 %2").arg(temperature).arg(unitText));

    auto qdt = QDateTime::currentSecsSinceEpoch();
    _readEntries.insert(qdt, QString("%1,%2,%3,%4").arg(qdt)
                                                   .arg(ui->ppmCo2ValueLabel->text())
                                                   .arg(temperature)
                                                   .arg(unitText));
    refreshTable();

    _ppmPresent = false;

    emit unsavedData(true);
}

void MainLayout::handlePPM(QString ppm)
{
    ui->ppmCo2ValueLabel->setText(ppm);
    _ppmPresent = true;
}

void MainLayout::handleSerialPortError(QString & error)
{
    QMessageBox::critical(this, tr("Serial port error"),
                          tr("An error has occurred with the serial "
                             "port. The error is: %1").arg(error));
}

void MainLayout::saveReadings()
{
    if(_fileName.isEmpty())
    {
        _fileName = QString("%1/%2.csv").arg(QStandardPaths::writableLocation(QStandardPaths::StandardLocation::DocumentsLocation))
                                        .arg(QDateTime::currentDateTime().toString(Qt::ISODate));

        _fileName = QFileDialog::getSaveFileName(this, tr("Generate CSV"),
                                                 _fileName,
                                                 tr("CSV (*.csv *.CSV)"));
    }

    QFile file(_fileName);

    if(file.open(QFile::WriteOnly | QFile::Truncate))
    {
        QTextStream out(&file);
        QMapIterator<qint64, QString> i(_readEntries);

        while(i.hasNext())
        {
            i.next();
            out << i.value() << QStringLiteral("\r\n");
        }

        file.flush();
        file.close();

        _unsavedData = false;
        emit unsavedData(false);
    }
    else
    {
        QMessageBox::warning(this,
                             tr("Could not save file"),
                             tr("An error has occured while trying to save the file %1").arg(_fileName));
        _fileName.clear();
    }
}
